"""
MIT License

Copyright (c) 2020 LightSage

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import discord

import objects

# plural uses code provided by Rapptz under the MIT License
# © 2015 Rapptz
# https://github.com/Rapptz/RoboDanny/blob/6fd16002e0cbd3ed68bf5a8db10d61658b0b9d51/cogs/utils/formats.py
class plural: # noqa
    def __init__(self, value):
        self.value = value

    def __format__(self, format_spec):
        v = self.value
        singular, sep, plural = format_spec.partition('|')
        plural = plural or f'{singular}s'
        if abs(v) != 1:
            return f'{v} {plural}'
        return f'{v} {singular}'


def truncate_text(text: str, limit: int, *, suffix: str = "...") -> str:
    if len(text) < limit:
        return text
    return text[:limit - len(suffix)] + suffix


class Formatter:
    """Basic formatter class.

    This is meant to be subclassed so you can add your own formatters.

    Attributes
    ----------
    payload :
        The class of the payload
    """

    def __init__(self, payload):
        self.payload = payload

    def tag_push(self):
        raise NotImplementedError

    def push(self):
        raise NotImplementedError

    def issue(self):
        raise NotImplementedError

    def merge_request(self):
        raise NotImplementedError


class GithubFormatter(Formatter):
    def __init__(self, payload):
        super().__init__(payload)

    def format_commits(self) -> list:
        previews = []
        for commit in self.payload.commits:
            commit_title = truncate_text(commit.title, 50)
            text = f"[`{commit.short_hash}`]({commit.url}) {discord.utils.escape_markdown(commit_title)} "\
                   f"- {commit.author.name}"
            previews.append(text)
        return previews

    def tag_push(self):
        title = f"[{self.payload.project.path_with_namespace}] New tag created: {self.payload.branch}"
        embed = discord.Embed(title=title)
        embed.set_author(name=self.payload.commit_author.name, icon_url=self.payload.commit_author.avatar_url)
        return embed

    def push(self):
        title = f"[{self.payload.project.name}:{self.payload.branch}] "\
                f"{plural(self.payload.total_commits_count):new commit}"

        if len(self.payload.commits) > 1:
            url = f"{self.payload.project.web_url}/-/compare/{self.payload.before}..."\
                  f"{self.payload.after}"
        else:
            url = self.payload.commits[0].url

        embed = discord.Embed(title=title, color=discord.Color.blurple(), url=url)

        if self.payload.commit_author.url is not None:
            embed.set_author(name=self.payload.commit_author.name, url=self.payload.commit_author.url,
                             icon_url=self.payload.commit_author.avatar_url)
        else:
            embed.set_author(name=self.payload.commit_author.name,
                             icon_url=self.payload.commit_author.avatar_url)

        embed.description = truncate_text("\n".join(self.format_commits()), 2048)
        return embed

    def issue(self):
        if self.payload.action == objects.IssueAction.open:
            title = f"[{self.payload.project.path_with_namespace}] Issue opened: #{self.payload.iid} "\
                    f"{self.payload.title}"
            description = self.payload.description
            color = 15426592
            embed = discord.Embed(title=title, color=color, description=description)
        elif self.payload.action == objects.IssueAction.update:
            return None
        elif self.payload.action == objects.IssueAction.close:
            title = f"[{self.payload.project.path_with_namespace}] Issue closed: #{self.payload.iid} "\
                    f"{self.payload.title}"
            embed = discord.Embed(title=title)
        elif self.payload.action == objects.IssueAction.reopen:
            title = f"[{self.payload.project.path_with_namespace}] Issue reopened: #{self.payload.iid} "\
                    f"{self.payload.title}"
            embed = discord.Embed(title=title)
        embed.url = self.payload.url
        embed.set_author(name=self.payload.user.name, url=self.payload.user.url, icon_url=self.payload.user.avatar_url)
        return embed

    def merge_request(self):
        if self.payload.action == objects.MergeRequestAction.open:
            title = f"[{self.payload.project.path_with_namespace}] Merge request opened: !{self.payload.iid} "\
                    f"{self.payload.title}"
            description = truncate_text(self.payload.description, 2000)
            color = 0x38912
            embed = discord.Embed(title=title, color=color, description=description)
        elif self.payload.action == objects.MergeRequestAction.update:
            return None
        # Apparently github doesn't do anything special for the merged action
        elif self.payload.action in (objects.MergeRequestAction.close, objects.MergeRequestAction.merged):
            title = f"[{self.payload.project.path_with_namespace}] Merge request closed: !{self.payload.iid} "\
                    f"{self.payload.title}"
            embed = discord.Embed(title=title)
        elif self.payload.action == objects.MergeRequestAction.reopen:
            title = f"[{self.payload.project.path_with_namespace}] Merge request reopened: !{self.payload.iid} "\
                    f"{self.payload.title}"
            embed = discord.Embed(title=title)
        embed.url = self.payload.url
        embed.set_author(name=self.payload.user.name, url=self.payload.user.url, icon_url=self.payload.user.avatar_url)
        return embed
