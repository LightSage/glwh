"""
MIT License

Copyright (c) 2020 LightSage

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from starlette.routing import Route
from starlette.exceptions import HTTPException
from starlette.responses import JSONResponse, PlainTextResponse
import discord

import objects
import config


class PayloadException(Exception):
    pass


def _push_transformer(obj):
    if len(obj.commits) == 0:
        raise PayloadException("Payload contains no commits")
    return obj.formatter.push()


def _tag_push_transformer(obj):
    return obj.formatter.tag_push()


def _issue_transformer(obj):
    return obj.formatter.issue()


def _mergerequest_transformer(obj):
    return obj.formatter.merge_request()


EVENT_CLASS = {"push": (objects.PushEvent, _push_transformer),
               "tag_push": (objects.PushEvent, _tag_push_transformer),
               "issue": (objects.IssueEvent, _issue_transformer),
               "merge_request": (objects.MergeRequestEvent, _mergerequest_transformer)}


def determine_args(value):
    val = {'username': config.default_webhook_name, 'avatar_url': config.webhook_avatar}
    if isinstance(value, dict):
        val.update(value)
    elif isinstance(value, str):
        val.update({'content': value, 'embed': None})
    elif isinstance(value, discord.Embed):
        val.update({'embed': value, 'content': None})
    return val


async def post_receive(request):
    webhook_id = request.path_params['webhook_id']
    webhook_token = request.path_params['webhook_token']

    if not request.headers['Content-Type'] == "application/json":
        raise HTTPException(400, "Content-Type is not \"application/json\".")

    payload = await request.json()
    payload['event'] = '_'.join(request.headers.get("X-Gitlab-Event", "").lower()[:-4].split())
    base = EVENT_CLASS.get(payload['event'])
    if base is None:
        raise HTTPException(501, f"\"{payload['event']}\" is not supported.")
    obj = base[0](payload)
    fmt = base[1](obj)
    webhook = discord.Webhook.partial(webhook_id, webhook_token,
                                      adapter=discord.AsyncWebhookAdapter(request.app.aiohttp_session))
    try:
        await webhook.send(**determine_args(fmt))
    except discord.HTTPException as e:
        raise HTTPException(e.response, e.status)
    return JSONResponse()


async def generalpage(request):
    return PlainTextResponse("Hello, world!")


all_routes = [Route('/post/{webhook_id:int}/{webhook_token:str}', post_receive, methods=['POST']),
              Route('/', generalpage)]
