# GLWH

Receives webhook events from GitLab and dispatches them to Discord


## Supported Events
- Push
- Tags
- Issues
- Merge Requests


### Basic Instructions

Install requirements `pip install -U -r requirements.txt`

Configure the config file and rename it to `config.py`

Run `hypercorn main:app`

Make a webhook in Discord and copy the webhook id and token.

Setup a webhook in your gitlab repository. 
`https://<yourhost>/post/<webhook_id>/<webhook_token>`