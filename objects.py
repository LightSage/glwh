"""
MIT License

Copyright (c) 2020 LightSage

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import dateutil.parser

from datetime import datetime

from formatters import GithubFormatter

import enum

import config


class Events(object):
    push = 1
    tag_push = 2


class Project(object):
    """Represents a project"""
    __slots__ = ("id", "name", "description", "web_url", "avatar_url", "git_ssh_url", "git_http_url", "namespace",
                 "visibility_level", "path_with_namespace", "default_branch", "homepage", "url", "ci_config_path")

    def __init__(self, payload):
        self.id = payload.get("id")
        self.name = payload.get("name")
        self.description = payload.get("description")
        self.web_url = payload.get("web_url")
        self.avatar_url = payload.get("avatar_url")
        self.git_ssh_url = payload.get("git_ssh_url")
        self.git_http_url = payload.get("git_http_url")
        self.namespace = payload.get("namespace")
        self.visibility_level = payload.get("visibility_level", 0)
        self.path_with_namespace = payload.get("path_with_namespace")
        self.default_branch = payload.get("default_branch")
        self.homepage = payload.get("homepage")
        self.url = payload.get("url")
        self.ci_config_path = payload.get("ci_config_path")

    @property
    def ssh_url(self):
        return self.git_ssh_url

    @property
    def http_url(self):
        return self.git_http_url


class Repository(object):
    def __init__(self, payload):
        self.name = payload.get("name")
        self.url = payload.get("url")
        self.description = payload.get("description", None)
        self.homepage = payload.get("homepage")
        self.git_http_url = payload.get("git_http_url")
        self.git_ssh_url = payload.get("git_ssh_url")
        self.visibility_level = payload.get("visibility_level", 0)


class Commit(object):
    __slots__ = ("id", "message", "title", "_timestamp", "url", "author", "added", "modified", "removed")

    def __init__(self, payload):
        self.id = payload.get("id")
        self.message = payload.get("message")
        self.title = payload.get("title")
        self._timestamp = payload.get("timestamp")
        self.url = payload.get("url")
        self.author = PartialUser(payload['author'])

        self.added = payload.get("added", [])
        self.modified = payload.get("modified", [])
        self.removed = payload.get("removed", [])

    @property
    def short_hash(self):
        return self.id[:7]

    @property
    def hash(self):
        return self.id

    @property
    def timestamp(self):
        return dateutil.parser.parse(self._timestamp)

    def __repr__(self):
        return f"<Commit id={self.id}>"


class PartialUser(object):
    def __init__(self, payload):
        self.name = payload.get("name")
        self.email = payload.get("email")


class User(PartialUser):
    def __init__(self, payload):
        super().__init__(payload)
        self.username = payload.get("username")
        self.avatar_url = payload.get("avatar_url")
        self.url = f"{config.gitlab_domain}/{self.username}"


class Committer(object):
    __slots__ = ("name", "id", "username", "email", "avatar_url", "url")

    def __init__(self, payload):
        self.name = payload.get("user_name")
        self.id = payload.get("user_id")
        self.username = payload.get("user_username")
        self.email = payload.get("user_email")
        self.avatar_url = payload.get("user_avatar")
        self.url = f"{config.gitlab_domain}/{self.username}" if self.username else None


class LabelType(enum.Enum):
    ProjectLabel = 1
    GroupLabel = 2


class Label(object):
    __slots__ = ("id", "title", "color", "project_id", "_created_at", "_updated_at", "template", "description",
                 "type", "group_id")

    def __init__(self, payload):
        self.id = payload.get('id')
        self.title = payload.get('title')
        self.color = payload.get('color')
        self.project_id = payload.get('project_id')
        self.template = payload.get('template')
        self.description = payload.get('description')

        self.type = LabelType[payload['type']] if 'type' in payload else None

        self.group_id = payload.get('group_id')

        self._created_at = payload.get('created_at', datetime.utcnow())
        self._updated_at = payload.get('updated_at', datetime.utcnow())

    @property
    def name(self):
        return self.title

    @property
    def created_at(self):
        return dateutil.parser.parse(self._created_at)

    @property
    def updated_at(self):
        return dateutil.parser.parse(self._updated_at)


class BaseEvent(object):
    __slots__ = ("object_kind", "project", "event", "formatter")

    def __init__(self, payload, *, formatter=GithubFormatter):
        self.object_kind = payload["object_kind"]
        self.event = payload['event']
        # Project is present in all events except Job Events
        self.project = Project(payload.get('project', {}))

        self.formatter = formatter(self)

    def __repr__(self):
        return f"<BaseEvent event={self.event}>"


class PushEvent(BaseEvent):
    __slots__ = ("before", "after", "ref", "checkout_sha", "commit_author", "project_id", "repository",
                 "commits", "total_commits_count", "branch", "message", "push_options")

    def __init__(self, payload):
        super().__init__(payload)
        self.before = payload.get("before")
        self.after = payload.get("after")

        self.ref = payload.get("ref", "")
        self.branch = self.ref.replace("refs/heads/", "")

        self.checkout_sha = payload.get("checkout_sha")
        self.message = payload.get("message")
        self.commit_author = Committer(payload)
        self.project_id = payload.get("project_id")

        self.repository = Repository(payload['repository'])
        self.commits = sorted([Commit(commit) for commit in payload['commits']], key=lambda c: c.timestamp)

        self.push_options = payload.get("push_options", {})
        self.total_commits_count = payload.get("total_commits_count")


class IssueState(enum.Enum):
    opened = 1
    closed = 2


class IssueAction(enum.Enum):
    open = 1
    update = 2
    close = 3
    reopen = 4


def _transform_assignees(data):
    return [User(a) for a in data]


def _transform_labels(data):
    return [Label(t) for t in data]


class Changes(object):
    def __iter__(self):
        return iter(self.__dict__.items())


class IssueEvent(BaseEvent):

    TRANSFORMERS = {"title": (None), "description": (None), "discussion_locked": (None),
                    "assignees": (_transform_assignees), "labels": (_transform_labels)}

    __slots__ = ('_created_at', '_updated_at', 'action', 'after', 'assignee_id', 'assignee_ids', 'assignees',
                 'author_id', 'before', 'confidential', 'description', 'discussion_locked', 'due_date',
                 'duplicated_to_id', 'event', 'formatter', 'human_time_estimate',
                 'human_total_time_spent', 'id', 'iid', 'labels', '_last_edited_at', 'last_edited_by_id',
                 'milestone_id', 'moved_to_id', 'object_kind', 'project', 'relative_position', 'state', 'state_id',
                 'time_estimate', 'title', 'total_time_spent', 'updated_by_id', 'url', 'user', 'weight')

    def __init__(self, payload):
        super().__init__(payload)
        self.id = payload['object_attributes'].get("id")
        self.title = payload['object_attributes'].get("title")
        self.assignee_ids = payload['object_attributes'].get("assignee_ids", [])
        self.assignee_id = payload['object_attributes'].get("assignee_id")
        self.author_id = payload['object_attributes'].get("author_id")

        self._created_at = payload['object_attributes'].get("created_at")
        self._updated_at = payload['object_attributes'].get("_updated_at")

        self.updated_by_id = payload['object_attributes'].get("updated_by_id")
        self._last_edited_at = payload['object_attributes'].get("last_edited_at")
        self.last_edited_by_id = payload['object_attributes'].get("last_edited_by_id")
        self.relative_position = payload['object_attributes'].get("relative_position")
        self.description = payload['object_attributes'].get("description")
        self.milestone_id = payload['object_attributes'].get("milestone_id")
        self.state_id = payload['object_attributes'].get("state_id")
        self.confidential = payload['object_attributes'].get("confidential", False)
        self.discussion_locked = payload['object_attributes'].get("discussion_locked", False)
        self.due_date = payload['object_attributes'].get("due_date")
        self.moved_to_id = payload['object_attributes'].get("moved_to_id")
        self.duplicated_to_id = payload['object_attributes'].get("duplicated_to_id")
        self.time_estimate = payload['object_attributes'].get("time_estimate")
        self.total_time_spent = payload['object_attributes'].get("total_time_spent")
        self.human_total_time_spent = payload['object_attributes'].get("human_total_time_spent")
        self.human_time_estimate = payload['object_attributes'].get("human_time_estimate")
        self.weight = payload['object_attributes'].get("weight")
        self.iid = payload['object_attributes'].get("iid")
        self.url = payload['object_attributes'].get("url")
        self.state = IssueState[payload['object_attributes'].get("state")]
        self.action = IssueAction[payload['object_attributes']['action']]
        self.labels = [Label(lab) for lab in payload.get('labels', [])]

        self.assignees = [User(a) for a in payload.get('assignees', [])]

        # User
        self.user = User(payload['user'])

        self.before = Changes()
        self.after = Changes()

        changes = payload['changes']

        for element in changes:
            transformer = self.TRANSFORMERS.get(element)

            try:
                before = changes[element]['previous']
            except KeyError:
                before = None
            else:
                if transformer:
                    before = transformer(before)

            setattr(self.before, element, before)

            try:
                after = changes[element]['current']
            except KeyError:
                after = None
            else:
                if transformer:
                    after = transformer(after)

            setattr(self.after, element, after)

    @property
    def created_at(self):
        return dateutil.parser.parse(self._created_at)

    @property
    def updated_at(self):
        return dateutil.parser.parse(self._updated_at)

    @property
    def last_edited_at(self):
        return dateutil.parser.parse(self._last_edited_at)


class MergeRequestAction(enum.Enum):
    open = 1
    update = 2
    close = 3
    reopen = 4
    merged = 5
    merge = 5


class MergeRequestState(enum.Enum):
    opened = 1
    closed = 2
    merged = 3


def _transform_datetime(data):
    if data is not None:
        return dateutil.parser.parse(data)
    else:
        return None


class MergeRequestEvent(BaseEvent):

    TRANSFORMERS = {"title": (None), "description": (None), "discussion_locked": (None),
                    "assignees": (_transform_assignees), "labels": (_transform_labels),
                    "updated_at": (_transform_datetime), "updated_by_id": (None)}

    __slots__ = ("user", "id", "author_id", "_created_at", "description", "url", "title", "iid", "merge_params",
                 "merge_status", "merge_when_pipeline_succeeds", "source_branch", "source_project_id", "source",
                 "last_commit", "work_in_progress", "total_time_spent", "assignee_ids", "state", "action", "oldrev",
                 "before", "after", "labels", "assignees")

    def __init__(self, payload):
        super().__init__(payload)
        self.user = User(payload['user'])

        # Object Attributes
        attrs = payload.get("object_attributes", {})
        self.id = attrs.get("id")
        self.author_id = attrs.get("author_id")
        self._created_at = attrs.get("created_at")
        self.description = attrs.get("description", "")
        self.url = attrs.get("url", "")
        self.title = attrs.get("title", "")
        self.iid = attrs.get("iid")
        self.merge_params = attrs.get("merge_params", {})
        self.merge_status = attrs.get("merge_status")
        self.merge_when_pipeline_succeeds = attrs.get("merge_when_pipeline_succeeds", False)
        self.source_branch = attrs.get("source_branch")
        self.source_project_id = attrs.get("source_project_id")
        self.source = Project(attrs['source']) if "source" in attrs else None
        self.last_commit = Commit(attrs['last_commit']) if "last_commit" in attrs else None
        self.work_in_progress = attrs.get("work_in_progress")
        self.total_time_spent = attrs.get("total_time_spent")
        self.assignee_ids = attrs.get("assignee_ids", [])
        self.state = MergeRequestState[attrs['state']]
        self.action = MergeRequestAction[attrs['action']]
        self.oldrev = attrs.get("oldrev")

        # ---
        labels = payload.get("labels", [])
        self.labels = [Label(_label) for _label in labels]
        assignees = payload.get("assignees", [])
        self.assignees = [User(_user) for _user in assignees]

        changes = payload['changes']
        self.before = Changes()
        self.after = Changes()

        for element in changes:
            transformer = self.TRANSFORMERS.get(element)

            try:
                before = changes[element]['previous']
            except KeyError:
                before = None
            else:
                if transformer:
                    before = transformer(before)

            setattr(self.before, element, before)

            try:
                after = changes[element]['current']
            except KeyError:
                after = None
            else:
                if transformer:
                    after = transformer(after)

            setattr(self.after, element, after)

    @property
    def target(self):
        return self.project

    @property
    def created_at(self):
        return dateutil.parser.parse(self._created_at)
